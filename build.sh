#!/bin/sh

function oops {
    echo $*
    exit 1
}

function run {
    if [ -z "$npm_package_name" ]; then
        npx $@
    else
        $@
    fi
}

pristine=0

for arg in $*; do
    case $arg in
        --pristine )
            pristine=1 ;;
        * )
            oops "Unknown argument: $arg"
    esac
done

if [[ $pristine -ne 0 ]]; then
    test -z "$(git status --porcelain --untracked-files=no)" || oops "Cannot proceed with dirty working dir"
fi

run babel src --extensions '.js' --out-dir dist/cjs
