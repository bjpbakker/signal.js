// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {Signal} from "./signal.js";

/**
 * Get a signal that yields a high-res timestamp before each repaint. Typically
 * this signal yields at 60 fps.
 *
 * See: https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
 */
export const animationFrame = () => {
    const sig = Signal (performance.now());
    const tick = (t) => {
        sig.set (t);
        window.requestAnimationFrame (tick);
    };
    window.requestAnimationFrame (tick);
    return sig;
};

/**
 * Get a signal that yields true when the given key is pressed, and false when
 * released.
 *
 * A predicate can be provided to do additional filtering of the `keydown` and
 * `keyup` events (e.g. to check the state of modifier keys).
 */
export const keyPressed = (key, p=() => true) => {
    const sig = Signal (false);
    window.addEventListener ("keydown", (e) => {
        if (e.keyCode === key && p (e)) {
            sig.set (true);
        }
    });
    window.addEventListener ("keyup", (e) => {
        if (e.keyCode === key && p (e)) {
            sig.set (false);
        }
    });
    return sig;
};
