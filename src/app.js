// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {compose, curry, fanout, flip} from "@fpjs/overture/base";
import {equals, isSetoid} from "@fpjs/overture/algebras/setoid";
import {map} from "@fpjs/overture/algebras/functor";

import {Channel, receive, send} from "./channel.js";
import {dropRepeats, foldp, mergeFold, runSignal} from "./signal.js";

/**
 * Simplistic representation of an effectful computation, by enforcing lazy
 * evaluation. Mostly useful a fake "data constructor" to get an `() -> a`.
 */
///:: type Effect a = () -> a
///:: (a -> b) -> a -> Effect b
export const Eff = (f) => (x) => () => f(x);
/** Run an effect and void the result. */
///:: Effect a -> () -> ()
export const runEff = (eff) => (eff (), null);

/**
 * The result of applying foldp that describes how to transition to the next
 * state.
 */
///:: Transition { state :: a, effects :: Effect b }
export const Transition = (st, fx) => ({
    state: st,
    effects: fx,
    "fantasy-land/map": (f) => Transition (f (st), fx),
});

///:: a -> ()
const annul = () => null;

/** Lift a state `st` into a pure `Transition` (i.e. no effects). */
///:: (a) -> Transition a ()
export const noEffects = (st) => Transition(st, annul);
/** `Transition` by applying effects. */
///:: (Effect b) -> a -> Transition a b
export const onlyEffects = flip (curry (Transition));
/** `Transition` to a derived state by (optionally) applying effects. */
///:: (a -> b) -> (a -> Effect c) -> a -> Transition b c
export const transition = (f, g=((x) => annul)) => (st) => Transition (... fanout (f) (g) (st));

///:: type Input a = a -> ()
///:: type RenderState a b = { state :: a, input :: Input b }
///:: Config { initState :: a, inputs :: [Signal b], foldp :: a -> b -> Transition a b, render :: RenderState -> Effect () }
///:: App { state :: Signal a, events :: Signal b, input :: Input b }

///:: Config a b -> App a b
export const start = (config) => {
    const foldState = (transition, ev) => config.foldp (ev) (transition.state);
    const foldEvents = (evs) => (transition) => evs.reduce (foldState, transition);

    const maybe = (f) => (x) => x && f (x);
    const applyEffects = Eff((transition) => maybe (map (input)) (transition.effects ()));
    const stateEquals = isSetoid (config.initState) ? equals : (x) => (y) => x === y;

    const chan = Channel([]);
    const inputs = mergeFold (config.inputs || []);
    const events = mergeFold ([receive (chan), inputs && map ((ev) => [ev]) (inputs)]);
    const transitions = foldp (foldEvents) (noEffects (config.initState)) (events);
    const state = dropRepeats (stateEquals) (map ((transition) => transition.state) (transitions));

    const input = (ev) => runEff(send (chan) ([ev]));

    runSignal (map (applyEffects) (transitions));
    runSignal (map ((st) => config.render({state: st, input})) (state));

    return {
        events,
        state,
        input,
    };
};
