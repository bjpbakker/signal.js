// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {Signal} from "./signal.js";

const now = Date.now;

/**
 * Create a signal that yields the current time every `t' milliseconds.
 *
 * Note: this uses an interval handle that cannot be canceled.
 */
export const every = (t) => {
    const r = Signal (now ());
    setInterval(() => r.set (now ()), t);
    return r;
};
