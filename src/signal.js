// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

export const Signal = (init) => {
    let val = init;
    let subs = [];
    const signal = {
        "@@type": "Signal",
        "constructor": Signal,
        "subscribe": (sub) => {
            subs.push (sub);
            sub (val);
        },
        "get": () => val,
        "set": (newval) => {
            val = newval;
            subs.forEach ((sub) => sub (val));
        },
        "fantasy-land/map": (f) => map (f) (signal),
        "fantasy-land/ap": (ab) => ap (signal) (ab),
    };
    return signal;
}
Signal['fantasy-land/of'] = Signal;

export const subscribe = (sub) => (sig) => sig.subscribe (sub);

export const filter = (pred) => (sig) => {
    const r = Signal (sig.get ());
    const f = (x) => {
        if (pred (x)) {
            r.set (x);
        }
    };
    subscribe (f) (sig);
    return r;
};

export const map = (f) => (sig) => {
    const r = Signal (f (sig.get ()) );
    subscribe ((v) => r.set (f (v))) (sig);
    return r;
};

export const ap = (fa) => (fab) => {
    const r = Signal (fab.get () (fa.get ()));
    fab.subscribe ((ab) => r.set (ab ( fa.get ())));
    fa.subscribe ((a) => r.set (fab.get () (a)));
    return r;
};

export const merge = (x) => (y) => {
    const r = Signal (x.get ());
    subscribe (r.set) (y);
    subscribe (r.set) (x);
    return r;
};

export const mergeFold = (sigs) => sigs.reduce (
    (a, b) => a == null ? b : b == null ? a : merge (a) (b),
    null
);

export const foldp = (f) => (z) => (sig) => {
    let acc = z;
    const r = Signal (acc);
    const sub = (v) => {
        acc = f (v) (acc);
        r.set (acc);
    };
    subscribe (sub) (sig);
    return r;
};

export const sampleOn = (x) => (y) => {
    const r = Signal (y.get ());
    subscribe (() => r.set( y.get ())) (x);
    return r;
}

export const dropRepeats = (eq) => (sig) => {
    let current = sig.get ();
    const r = Signal (current);
    const sub = (x) => {
        if (! eq (current) (x)) {
            current = x;
            r.set (current);
        }
    };
    subscribe (sub) (sig);
    return r;
};

/**
 * Run an effectful signal. Since JS lacks awareness of effects, an effect here
 * is a function that is applied to unit.
 */
export const runSignal = (sig) => (subscribe ((eff) => eff()) (sig), {});
