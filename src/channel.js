// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {Signal} from "./signal.js";

export const Channel = (init) => {
    const chan = Signal (init);
    return {
        "@@type": "Channel",
        "send": chan.set,
        "receive": () => chan,
    };
};

export const channel = Channel;

export const send = (chan) => (val) => () => chan.send (val);
export const receive = (chan) => chan.receive ();

export default Channel;
