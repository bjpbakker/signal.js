#!/usr/bin/env node

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {map} from "@fpjs/overture/algebras/functor";

import {App, Time} from "@fpjs/signal";
const {start, Eff, transition, noEffects} = App;

const foldp = (event) => {
    switch (event) {
    case "INC":
        return transition((counter) => counter + 1);
    }
    return noEffects;
}

const main = () => {
    const config = {
        initState: 0,
        foldp: foldp,
        render: Eff(({state: counter}) => console.log(`Count ${counter}`)),
        inputs: [
            map (() => "INC") (Time.every (1000)),
        ]
    };

    start (config);
}

main();
