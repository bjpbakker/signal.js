// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import * as THREE from "three";

import {id, compose} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import * as L from "@fpjs/overture/control/lens";
import {patchBuiltins} from "@fpjs/overture/patches";

import {App, Signal, DOM, Time} from "@fpjs/signal";
const {start, Eff, noEffects, transition} = App;
const {runSignal, filter, foldp: foldSig, mergeFold, sampleOn} = Signal;

const _ = L.Record;

const ZERO = {type: "Zero"};
const FPS = (fps) => ({
    type: "FPS",
    fps
});
const RotateX = (clockwise) => ({
    type: "RotateX",
    direction: clockwise ? 1 : -1
});
const RotateY = (clockwise) => ({
    type: "RotateY",
    direction: clockwise ? 1 : -1
});

const foldp = (event) => {
    switch (event.type) {
    case "Zero":
        return noEffects;
    case "FPS":
        return transition(
            L.set (_.fps) (event.fps)
        );
    case "RotateX":
        return transition(
            L.over (_.rotation.x) ((x) => x + (0.02 * event.direction))
        );
    case "RotateY":
        return transition(
            L.over (_.rotation.y) ((y) => y + (0.02 * event.direction))
        );
    }
};

const countFPS = (frames) => {
    const count = (x) => ([last, frames, fps]) => {
        const delta = x - last;
        if (delta >= 1000) {
            return [x, 0, Math.round(frames / (delta / 1000))];
        }
        return [last, frames + 1, fps];
    };
    return map (([t,f,fps]) => FPS(fps)) (sampleOn (Time.every (1000)) (foldSig (count) ([0, 0, 0]) (frames)));
};

const initGL = () => {
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = 5;

    const renderer = new THREE.WebGLRenderer({antiAlias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.gammaInput = true;
    renderer.gammaOutput = true;

    return {camera, renderer};
}

const initScene = () => {
    const scene = new THREE.Scene();

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshPhongMaterial({color: 0x2194fc});
    const cube = new THREE.Mesh(geometry, material);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(1, 1, 1).normalize();

    scene.add(cube);
    scene.add(directionalLight);
    scene.add(new THREE.AmbientLight(0x222222));

    return {scene, cube};
};

const main = () => {
    const frames = DOM.animationFrame();

    const {camera, renderer} = initGL();
    const {scene, cube} = initScene();

    document.body.appendChild(renderer.domElement);

    const fpsEl = document.getElementById("fps");
    const renderState = Eff(({state, input}) => {
        fpsEl.textContent = `FPS: ${state.fps}`;

        cube.rotation.x = state.rotation.x;
        cube.rotation.y = state.rotation.y;
    });
    const renderGL = Eff(() => {
        camera.lookAt(scene.position);
        renderer.render(scene, camera);
    });

    const onKeyPress = (key) => (ev) => map (() => ev) (filter ((x) => x === true) (DOM.keyPressed (key)));
    const [LeftArrow, UpArrow, RightArrow, DownArrow] = [37, 38, 39, 40];
    const rotations = mergeFold([
        onKeyPress (LeftArrow) (RotateY(false)),
        onKeyPress (RightArrow) (RotateY(true)),
        onKeyPress (UpArrow) (RotateX(false)),
        onKeyPress (DownArrow) (RotateX(true)),
    ]);

    const config = {
        initState: {
            fps: 1,
            rotation: {x: 0, y: 0},
        },
        foldp: foldp,
        render: renderState,
        inputs: [rotations, countFPS(frames)]
    };

    start (config);
    runSignal (map (renderGL) (frames));
};

patchBuiltins();
main();
