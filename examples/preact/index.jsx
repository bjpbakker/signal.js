import { patchBuiltins } from "@fpjs/overture/patches";
import { App } from "@fpjs/signal";
const { Eff, start, noEffects, transition } = App;

import { render } from "preact";

const Application = ({state, input}) => (<span>Hello Signal!</span>);

const foldp = (event) => (state) => {
    return noEffects(state);
};

const run = (root) => {
    const view = Eff(({state, input}) => render(<Application state={state} input={input} />, root, root.lastElementChild));

    const initState = {
    };

    const config = {
        initState: initState,
        foldp: foldp,
        render: view,
        inputs: []
    };

    const app = start (config);
};

const main = () => {
    const root = document.getElementById("app");
    if (root != null) {
        run(root);
    } else {
        console.log("Waiting for app root to render..");
        setTimeout(main, 10);
    }
};

patchBuiltins ();
main ();
