// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import React, {Component} from "react";
import {createRoot} from "react-dom/client";

import {id, compose} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import * as L from "@fpjs/overture/control/lens";
import {patchBuiltins} from "@fpjs/overture/patches";

import {App, Channel} from "@fpjs/signal";
const {start, Eff, noEffects, transition} = App;

const _ = L.Record;

const ZERO = {type: "Zero"};

const SetSubject = (subject) => ({
    type: "SetSubject",
    subject,
});

const SetBody = (body) => ({
    type: "SetBody",
    body,
});

const GenerateSubject = () => ({
    type: "GenerateSubject",
});

const SetError = (e) => ({
    type: "SetError",
    message: e.message,
});

const Copy = (e) => ({
    type: "Copy",
    clipboardData: e.clipboardData,
});

const CommitFmt = ({state: {subject, subjectWarning, body, error}, input}) => (
    <div>
        <div className="heading">Format your commit messages</div>
        <div className="subject">
            <input type="text"
                   id="subject"
                   value={subject}
                   onChange={(e) => input(SetSubject(e.target.value))}
                   placeholder="Summarize your changes"
                   autoFocus
            />
            <span className="gen-subject" onClick={() => input(GenerateSubject())}>Generate</span>
            <span className="error">{error}</span>
            <span className="warn">{subjectWarning}</span>
        </div>
        <div className="body">
            <textarea value={body}
                      placeholder="Elaborate on your changes and provide context."
                      onChange={(e) => input(SetBody(e.target.value))}
            />
        </div>
        <div className="message">
            <span className="copy-hint">Use C-c to copy your message.</span>
            <p>{subject}</p>
            <p>{body}</p>
        </div>
    </div>
);

const wrapText = (len) => (s) => wrapLines(len)(s).join("\n");

const wrapLines = (len) => (s) => {
    if (s.length <= len) {
        return [s];
    }

    const eol = s.indexOf("\n");
    if (~eol && eol <= len) {
        return [s.substring(0, eol)].concat(wrapLines(len)(s.slice(eol + 1)));
    }

    const space = s.substring(0, len).lastIndexOf(" ");
    if (~space) {
        const line = s.substring(0, space);
        return [line].concat(wrapLines(len)(s.slice(space + 1)));
    }

    const firstSpace = s.indexOf(" ");
    if (~firstSpace) {
        const line = s.substring(0, firstSpace);
        return [line].concat(wrapLines(len)(s.slice(firstSpace + 1)));
    }

    return [s];
}

const filterOk = (res) => res.ok ? res : Promise.reject(new Error(`Request to '${res.url}' failed with HTTP status ${res.status}`));
const whatthecommit = () => fetch("/whatthecommit.txt").then(filterOk).then((res) => res.text());

const foldp = (event) => {
    switch (event.type) {
    case "Zero":
        return noEffects;
    case "SetSubject":
        return transition(L.setAll(
            [_.subject, event.subject],
            [_.subjectWarning, event.subject.length <= 50 ? "" : "Your subject exceeds 50 characters"]
        ));
    case "SetBody":
        const wrapBody = wrapText (80);
        return transition(
            L.set (_.body) (wrapBody(event.body))
        );
    case "SetError":
        return transition(
            L.set (_.error) (event.message)
        );
    case "Copy":
        return transition(
            id,
            Eff((state) => event.clipboardData.setData("text/plain", [state.subject, state.body].filter(id).join("\n\n"))),
        );
    case "GenerateSubject":
        return transition(
            L.set (_.error) (""),
            Eff(() => whatthecommit().then(SetSubject, SetError))
        );
    }
};

const main = () => {
    const copies = Channel.channel(ZERO);
    document.addEventListener('copy', (e) => {
        if (document.getSelection().toString() !== ""
            || document.activeElement.selectionStart != document.activeElement.selectionEnd) {
            return;
        }

        e.preventDefault();
        Channel.send (copies) (Copy(e)) ();
    });

    const root = createRoot(document.getElementById("app"));
    const config = {
        initState: {subject: "", body: ""},
        foldp: foldp,
        render: Eff(({state, input}) => root.render(<CommitFmt state={state} input={input} />)),
        inputs: [Channel.receive (copies)]
    };

    start (config);
};

patchBuiltins();
main();
