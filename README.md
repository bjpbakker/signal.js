# Signal.js

Signal.js provides ELM-style FRP for Javascript. It is heavily inspired by (an
earlier version of) [ELM](https://elm-lang.org/) and [Purescript
Signal](https://github.com/bodil/purescript-signal) and
[Pux](http://purescript-pux.org/).

## Upgrading

A changelog is kept in `RELEASES.md`, which lists the major changes.

## Getting Started

Install the latest version of Signal (`npm install @fpjs/signal`) and import it.

```js
import {App} from "@fpjs/signal";
const {start, noEffects, Eff} = App;

// Return a new state and possibly effects based on the event.
const update = (event) => (state) => {
    return noEffects (state);
};

// Render the state. New events can be emitted using input.
const view = Eff(({state, input}) => ...);

const config = {
  initState: InitialState,
  foldp: update,
  render: view,
  inputs: [],
};
const app = start (config);
```

### Lenses

Using lenses from `@fpjs/overture` allows for cleaner use of transitions. The
following example shows incrementing a counter.

```js
import {over, Record} from "@fpjs/overture/control/lens";
import {App} from "@fpjs/signal";
const {transition, noEffects} = App;

const _ = Record;

const initialState = { counter: 0 };

const Increment = () => ({type: "Increment"});

const update = (event) => {
    switch (event.type) {
    case "Increment":
        return transition(
            over (_.counter) ((n) => n + 1)
        );
    default:
        return noEffects;
    }
};
```

### Events as actions

Instead of using an object data structure, events can also be represented as
functions. This is somewhat similar to [Mobx](https://mobx.js.org/) Actions.

```js
import {apply} from "@fpjs/overture/base";

const Increment = () => transition(over (_.counter) ((n) => n + 1));

const update = apply;
```

## Examples

The `examples` folder contains usage examples for different contexts.

`counter.js`
: command line counter built with Nodejs
`commitfmt`
: browser example built with React
`cube`
: webgl example built with Three.js
`preact`
: trivial example to show Preact setup

### Counter.js

The `counter.js` example is an executable javscript file that uses modern import
syntax.

```
> ./examples/counter.js
```

### Commitfmt

This example shows Signal for managing the state of a React web app. Open the
URL in a browser window by pressing `o` in the `vite` terminal. Or follow the
URL printed in the terminal.

```
> pushd examples/commitfmt
> npm install
> npm start
^C
> popd
```

### Cube

This examples shows Signal for managing state and FPS of a WebGL app. Open the
URL in a browser window by pressing `o` in the `vite` terminal. Or follow the
URL printed in the terminal. You can use the arrows on your keyboard to rotate
the cube.

```
> pushd examples/cube
> npm install
> npm start
^C
> popd
```

### Preact

This is a trivial example that contains all the moving parts to setup Signal
with Preact. Open the URL in a browser window by pressing `o` in the `vite`
terminal. Or follow the URL printed in the terminal.

```
> pushd examples/cube
> npm install
> npm start
^C
> popd
```

## License

Copyright (C) Bart Bakker.

This software is subject to the terms of the Mozilla Public License, v. 2.0. If
a copy of the MPL was not distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.
