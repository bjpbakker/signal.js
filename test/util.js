// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {map} from "@fpjs/overture/algebras/functor";

import {Signal, runSignal} from "@fpjs/signal/signal";
import {assert} from "chai";

const expect = (sig) => (values) => (done) => {
    let remaining = values;
    const next = (val) => () => {
        assert.equal (val, remaining.shift());
        if (remaining.length === 0) {
            done();
        }
    };
    return runSignal (map (next) (sig));
};

const tick = (vals) => (interval) => {
    let xs = vals.slice();
    const sig = Signal (xs.shift());
    setTimeout(
        function next() {
            sig.set (xs.shift());
            if (xs.length) {
                setTimeout(next, interval);
            }
        },
        interval
    );
    return sig;
};

const range = (n) => tick ([...new Array(n).keys()]);

export {
    expect, tick, range
};
