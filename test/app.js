// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";
import {assert} from "chai";

import {apply} from "@fpjs/overture/base";
import {Identity} from "@fpjs/overture/data/identity";

import {Eff, runEff, noEffects, onlyEffects, start, transition} from "@fpjs/signal/app";
import {expect} from "./util.js";

describe ("start", function() {
    const nextTick = (eff) => setTimeout(() => runEff(eff), 1);

    context ("simple counter", () => {
        it ("processes state transitions", (done) => {
            const config = {
                initState: 0,
                foldp: (ev) => transition((state) => state + ev),
                render: Eff(() => []),
            };
            const app = start (config);
            setTimeout(() => app.input(1), 1);
            setTimeout(() => app.input(1), 2);
            expect (app.state) ([0, 1, 2]) (done);
        });

        it ("renders state", () => {
            let rendered = [];
            const config = {
                initState: 0,
                foldp: (ev) => transition((state) => state + ev),
                render: Eff(({state}) => rendered.push(state)),
            };
            const app = start (config);
            app.input(1);
            app.input(1);
            assert.deepEqual([0, 1, 2], rendered);
        });

        it ("runs effects", () => {
            let rendered = [];
            const config = {
                initState: 1,
                foldp: (ev) => transition((state) => ev, Eff((state) => state < 10 ? Identity(state * 2) : null)),
                render: Eff(({state}) => rendered.push(state)),
            };
            const app = start (config);
            app.input(1);
            assert.deepEqual([1,2,4,8,16], rendered);
        });
    });

    context ("foldp = apply", () => {
        it ("processes state transitions", (done) => {
            const plus = (a) => (b) => noEffects(a + b);
            const config = {
                initState: 0,
                foldp: apply,
                render: Eff(() => []),
            };
            const app = start (config);
            app.input(plus(1));
            app.input(plus(2));
            expect (app.state) ([3]) (done);
        });
    });
});

describe ("onlyEffects", function() {
    it ("yield identical state", () => {
        const state = 1;
        const result = onlyEffects (() => []) (state);
        assert.equal(1, result.state);
        assert.deepEqual([], result.effects ());
    });
});
