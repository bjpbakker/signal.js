// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";

import {channel, send, receive} from "@fpjs/signal/channel";
import {expect} from "./util.js";

describe("Channel", function() {
    const runInput = (chan) => (val) => send (chan) (val) ();

    it ("yields sent values to a signal", function(done) {
        const chan = channel ("x");
        expect ( receive (chan) ) (["x", "y", "z"]) (done);
        runInput (chan) ("y");
        runInput (chan) ("z");
    });
});
