// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {describe, it} from "mocha";

import {map} from "@fpjs/overture/algebras/functor";
import {ap, lift, pure} from "@fpjs/overture/algebras/applicative";

import {
    Signal, dropRepeats, filter, foldp, merge, runSignal, sampleOn
} from "@fpjs/signal/signal";
import {expect, tick, range} from "./util.js";

describe ("Signal", function() {
    const x = Signal ("x");
    const y = Signal ("y");

    describe ("is a Functor", function() {
        it ("map a signal's value", function(done) {
            expect (map ((x) => x * 2) (range (4) (1))) ([0, 2, 4, 6]) (done);
        });
    });

    describe ("is an Applicative", function() {
        it ("supports lift", function(done) {
            const inc = (n) => n + 1;
            expect (lift (inc) (range (2) (1))) ([1, 2]) (done);
        });

        it ("lifts a value with pure", function(done) {
            expect (pure (Signal) (1)) ([1]) (done);
        });
    });

    describe ("filter", function() {
        it ("yields only values that satisfy a predicate", function(done) {
            const even = (n) => n % 2 === 0;
            expect (filter (even) (range (6) (1))) ([0, 2, 4]) (done);
        });
    });

    describe ("merge", function() {
        it ("yields first for two constant signals", function(done) {
            expect (merge (x) (y)) (["x"]) (done);
        });

        it ("yields first init and all rest for two signals", function(done) {
            const xs = tick (["x", "x", "x"]) (1);
            const ys = tick (["y", "y", "y"]) (1);
            expect (merge (xs) (ys)) (["x", "x", "y", "x", "y"]) (done);
        });
    });

    describe ("foldp", function() {
        it ("yields past dependend values", function(done) {
            const mult = (a) => (b) => a * b;
            const sig = foldp (mult) (1) (map ((n) => n + 1) (range (5) (1)));
            expect (sig) ([1, 2, 6, 24, 120]) (done);
        });
    });

    describe ("sampleOn", function() {
        it ("yields current b when signal a yields", function(done) {
            const as = tick (["a", "a", "a"]) (1);
            const bs = Signal (["b"]);
            expect (sampleOn (as) (bs)) (["b", "b", "b"]) (done);
        });
    });

    describe ("dropRepeats", function() {
        it ("yields non-repeated values", function(done) {
            const ns = tick ([1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 1]) (1);
            const eq = (a) => (b) => a === b;
            expect (dropRepeats (eq) (ns)) ([1, 2, 3, 4, 1]) (done);
        });
    });
});
