# Releases

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.5.0] - 2023-10-22

### Fixed

- Fixed `Signal` to be a full `Applicative`
- Fixed all examples and updated instructions to run them.

### Changed

- Require nodejs 16.x or higher (possibly breaking)
- Gitlab: run CI on all supported node versions (16, 18, 19, 20, 21)
- Examples now use `vite`

### Removed

- Remove `constant` from `Signal`; replace it with either `Signal` or using
  `Applicative`

## [v0.4.1] - 2022-05-13

### Fixed

- Exports definition for root module

## [v0.4.0] - 2022-05-11

### Changed

- Require nodejs 12.x or higher (possibly breaking)
- Change package type to `module` (possibly breaking)
- Replace babel root-imports with the package name
- Move cjs build into `dist/cjs`

## [v0.3.3] - 2021-03-10

### Changed

- Rename default branch to `main`
- Use `overture.js` 0.4.x

## v0.3.2 - 2020-01-17

### Fixed

- README: `overture.js` imports shown in snippets

### Changed

- DOM: `keyPressed` takes optional predicate

## v0.3.1 - 2020-01-03

### Added

- Example using [preact](https://preactjs.com/)

## v0.3.0 - 2019-09-13

### Fixed

- README: imports shown in snippets

### Changed

- Use `overture.js` 0.3.0

## v0.2.0 - 2019-08-02

### Changed

- Export all modules through `@fpjs/signal`

## v0.1.4 - 2019-08-02

### Added

- Publish both sources (`src/`) and transpiled code (`lib/`)

## v0.1.3 - 2019-06-07

### Changed

- README: describe using lenses

## v0.1.2 - 2019-06-07

### Changed

- Cleanup and added tests

### Removed

- Examples from published package

## v0.1.1 - 2019-05-31

Initial release.

### Added

- Algebras: `Setoid`, `Functor`, `Applicative`, `Chain`
- Data types: `Identity`, `Const`
- Algebras for builtins: `Array`, `Function`, `Promise`
- Lenses

[v0.3.3]: https://gitlab.com/bjpbakker/signal.js/-/tags/v0.3.3
[v0.4.0]: https://gitlab.com/bjpbakker/signal.js/-/compare/v0.3.3...v0.4.0
[v0.4.1]: https://gitlab.com/bjpbakker/signal.js/-/compare/v0.4.0...v0.4.1
[v0.5.0]: https://gitlab.com/bjpbakker/signal.js/-/compare/v0.4.1...v0.5.0
[unstable]: https://gitlab.com/bjpbakker/signal.js/-/compare/v0.5.0...main
